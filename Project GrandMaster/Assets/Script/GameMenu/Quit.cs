﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Microsoft.MixedReality.USYD.GameMenu
{
    public class Quit : MonoBehaviour
    {
        public void QuitOnClick()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
